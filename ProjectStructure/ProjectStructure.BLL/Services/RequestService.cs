﻿using AutoMapper;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.DataAccess.Interfaces;
using ProjectStructure.Shared.DTO.Linq;
using ProjectStructure.Shared.DTO.Task;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services
{
    public class RequestService : IRequestService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public RequestService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public int FirstTask()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<TaskDTO> SecondTask()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<string> ThirdTask()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<string> FourthTask()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<string> FifthTask()
        {
            throw new NotImplementedException();
        }

        public SixthTaskDTO SixthTask()
        {
            throw new NotImplementedException();
        }       

        public SeventhTaskDTO SeventhTask()
        {
            throw new NotImplementedException();
        }
    }
}
