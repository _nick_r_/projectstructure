﻿using AutoMapper;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.DataAccess.Interfaces;
using ProjectStructure.DataAccess.Models;
using ProjectStructure.Shared.DTO.Team;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services
{
    public class TeamService : ITeamService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public TeamService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public TeamDTO AddTeam(TeamDTO team)
        {
            Team teamModel = mapper.Map<TeamDTO, Team>(team);
            return mapper.Map<Team, TeamDTO>(unitOfWork.Teams.Create(teamModel));
        }

        public void DeleteTeam(int id)
        {
            try
            {
                unitOfWork.Teams.Delete(id);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public List<TeamDTO> GetAllTeams()
        {
            IEnumerable<Team> teams = unitOfWork.Teams.GetList();
            return mapper.Map<IEnumerable<Team>, List<TeamDTO>>(teams);
        }

        public TeamDTO GetTeam(int id)
        {
            Team team = unitOfWork.Teams.Get(id);
            return mapper.Map<Team, TeamDTO>(team);
        }

        public TeamDTO UpdateTeam(int id, TeamDTO team)
        {
            Team teamModel = mapper.Map<TeamDTO, Team>(team);
            Team teamUpd = unitOfWork.Teams.Update(id, teamModel);
            return mapper.Map<Team, TeamDTO>(teamUpd);
        }

    }
}
