﻿using AutoMapper;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Shared.DTO.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectStructure.DataAccess.Interfaces;
using ProjectStructure.DataAccess.Models;

namespace ProjectStructure.BLL.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public UserService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public UserDTO AddUser(UserDTO user)
        {
            User userModel = mapper.Map<UserDTO, User>(user);
            return mapper.Map<User, UserDTO>(unitOfWork.Users.Create(userModel));
        }

        public void DeleteUser(int id)
        {
            try
            {
                unitOfWork.Users.Delete(id);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public List<UserDTO> GetAllUsers()
        {
            IEnumerable<User> users = unitOfWork.Users.GetList();
            return mapper.Map<IEnumerable<User>, List<UserDTO>>(users);
        }

        public UserDTO GetUser(int id)
        {
            User user = unitOfWork.Users.Get(id);
            return mapper.Map<User, UserDTO>(user);
        }

        public UserDTO UpdateUser(int id, UserDTO user)
        {
            User userModel = mapper.Map<UserDTO, User>(user);
            User userUpd = unitOfWork.Users.Update(id, userModel);
            return mapper.Map<User, UserDTO>(userUpd);
        }
    }
}
