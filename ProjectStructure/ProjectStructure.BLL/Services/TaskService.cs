﻿using AutoMapper;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.DataAccess.Interfaces;
using ProjectStructure.Shared.DTO.Task;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services
{
    public class TaskService : ITaskService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public TaskService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public TaskDTO AddTask(TaskDTO task)
        {
            DataAccess.Models.Task taskModel = mapper.Map<TaskDTO, DataAccess.Models.Task>(task);
            return mapper.Map<DataAccess.Models.Task, TaskDTO>(unitOfWork.Tasks.Create(taskModel));
        }

        public void DeleteTask(int id)
        {
            try
            {
                unitOfWork.Tasks.Delete(id);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public List<TaskDTO> GetAllTasks()
        {
            IEnumerable<DataAccess.Models.Task> task = unitOfWork.Tasks.GetList();
            return mapper.Map<IEnumerable<DataAccess.Models.Task>, List<TaskDTO>>(task);
        }

        public TaskDTO GetTask(int id)
        {
            DataAccess.Models.Task task = unitOfWork.Tasks.Get(id);
            return mapper.Map<DataAccess.Models.Task, TaskDTO>(task);
        }

        public TaskDTO UpdateTask(int id, TaskDTO task)
        {
            DataAccess.Models.Task taskModel = mapper.Map<TaskDTO, DataAccess.Models.Task>(task);
            DataAccess.Models.Task taskUpd = unitOfWork.Tasks.Update(id, taskModel);
            return mapper.Map<DataAccess.Models.Task, TaskDTO>(taskUpd);
        }
    }
}
