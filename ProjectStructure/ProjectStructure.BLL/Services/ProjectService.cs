﻿using AutoMapper;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.DataAccess.Interfaces;
using ProjectStructure.DataAccess.Models;
using ProjectStructure.Shared.DTO.Project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services
{
    public class ProjectService : IProjectService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public ProjectService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public ProjectDTO AddProject(ProjectDTO project)
        {
            Project projectModel = mapper.Map<ProjectDTO, Project>(project);
            return mapper.Map<Project, ProjectDTO>(unitOfWork.Projects.Create(projectModel));
        }

        public void DeleteProject(int id)
        {
            try
            {
                unitOfWork.Projects.Delete(id);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public List<ProjectDTO> GetAllProjects()
        {
            IEnumerable<Project> projects = unitOfWork.Projects.GetList();
            return mapper.Map<IEnumerable<Project>, List<ProjectDTO>>(projects);
        }

        public ProjectDTO GetProject(int id)
        {
            Project project = unitOfWork.Projects.Get(id);
            return mapper.Map<Project, ProjectDTO>(project);
        }

        public ProjectDTO UpdateProject(int id, ProjectDTO project)
        {
            Project projectModel = mapper.Map<ProjectDTO, Project>(project);
            Project projectUpd = unitOfWork.Projects.Update(id, projectModel);
            return mapper.Map<Project, ProjectDTO>(projectUpd);
        }

    }
}
