﻿using ProjectStructure.Shared.DTO.Task;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Interfaces
{
    public interface ITaskService
    {
        TaskDTO AddTask(TaskDTO aircraft);
        List<TaskDTO> GetAllTasks();
        TaskDTO GetTask(int id);
        TaskDTO UpdateTask(int id, TaskDTO aircraft);
        void DeleteTask(int id);
    }
}
