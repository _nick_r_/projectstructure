﻿using ProjectStructure.Shared.DTO.Team;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Interfaces
{
    public interface ITeamService
    {
        TeamDTO AddTeam(TeamDTO aircraft);
        List<TeamDTO> GetAllTeams();
        TeamDTO GetTeam(int id);
        TeamDTO UpdateTeam(int id, TeamDTO aircraft);
        void DeleteTeam(int id);
    }
}
