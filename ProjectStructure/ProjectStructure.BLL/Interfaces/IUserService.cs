﻿using ProjectStructure.Shared.DTO.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ProjectStructure.BLL.Interfaces
{
    public interface IUserService
    {
        UserDTO AddUser(UserDTO user);
        List<UserDTO> GetAllUsers();
        UserDTO GetUser(int id);
        UserDTO UpdateUser(int id, UserDTO user);
        void DeleteUser(int id);
    }
}
