﻿using ProjectStructure.Shared.DTO.Project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Interfaces
{
    public interface IProjectService
    {
        ProjectDTO AddProject(ProjectDTO aircraft);
        List<ProjectDTO> GetAllProjects();
        ProjectDTO GetProject(int id);
        ProjectDTO UpdateProject(int id, ProjectDTO aircraft);
        void DeleteProject(int id);
    }
}
