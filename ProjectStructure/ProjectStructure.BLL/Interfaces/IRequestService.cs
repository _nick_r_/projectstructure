﻿using ProjectStructure.Shared.DTO.Linq;
using ProjectStructure.Shared.DTO.Task;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Interfaces
{
    public interface IRequestService
    {
        int FirstTask();
        IEnumerable<TaskDTO> SecondTask();
        IEnumerable<string> ThirdTask();
        IEnumerable<string> FourthTask();
        IEnumerable<string> FifthTask();
        SixthTaskDTO SixthTask();
        SeventhTaskDTO SeventhTask();

    }
}
