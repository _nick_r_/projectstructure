﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.DataAccess.Models;
using ProjectStructure.Shared.DTO.Team;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructure.DataAccess.Interfaces.Repositories;
using ProjectStructure.DataAccess.Interfaces;
using ProjectStructure.BLL.Interfaces;

namespace ProjectStructure.WebAPI.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly ITeamService teamService;

        public TeamsController(ITeamService teamService)
        {
            this.teamService = teamService;
        }

        [HttpGet]
        public ActionResult<ICollection<TeamDTO>> GetTeams()
        {
            return Ok(teamService.GetAllTeams());
        }

        [HttpPost]
        public ActionResult<TeamDTO> CreateTeam([FromBody] TeamDTO newTeam)
        {
            return Ok(teamService.AddTeam(newTeam));
        }

        [HttpPut("{id}")]
        public ActionResult<TeamDTO> UpdateTeam(int id, [FromBody] TeamDTO newTeam)
        {
            return Ok(teamService.UpdateTeam(id, newTeam));
        }

        [HttpDelete("{id:int}")]
        public IActionResult DeleteTeam(int id)
        {
            teamService.DeleteTeam(id);
            return NoContent();
        }

        [HttpGet("{id:int}")]
        public ActionResult<TeamDTO> GetTeam(int id)
        {
            return Ok(teamService.GetTeam(id));

        }
    }
}
