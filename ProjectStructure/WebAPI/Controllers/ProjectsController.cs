﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Shared.DTO.Project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectService projectService;

        public ProjectsController(IProjectService projectService)
        {
            this.projectService = projectService;
        }

        [HttpGet]
        public ActionResult<ICollection<ProjectDTO>> GetProjects()
        {
            return Ok(projectService.GetAllProjects());
        }

        [HttpPost]
        public ActionResult<ProjectDTO> CreateProject([FromBody] ProjectDTO newProject)
        {
            return Ok(projectService.AddProject(newProject));
        }

        [HttpPut("{id}")]
        public ActionResult<ProjectDTO> UpdateProject(int id, [FromBody] ProjectDTO newProject)
        {
            return Ok(projectService.UpdateProject(id, newProject));
        }

        [HttpDelete("{id:int}")]
        public IActionResult DeleteProject(int id)
        {
            projectService.DeleteProject(id);
            return NoContent();
        }

        [HttpGet("{id:int}")]
        public ActionResult<ProjectDTO> GetProject(int id)
        {
            return Ok(projectService.GetProject(id));

        }
    }
}
