﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.DataAccess.Interfaces;
using ProjectStructure.DataAccess.Interfaces.Repositories;
using ProjectStructure.DataAccess.Models;
using ProjectStructure.Shared.DTO.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace ProjectStructure.WebAPI.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService userService;

        public UsersController(IUserService userService)
        {
            this.userService = userService;
        }

        [HttpGet]
        public ActionResult<ICollection<UserDTO>> GetUsers()
        {
            return Ok(userService.GetAllUsers());        
        }

        [HttpPost]
        public ActionResult<UserDTO> CreateUser([FromBody] UserDTO newUser)
        {
            

            return Ok(userService.AddUser(newUser));
        }

        [HttpPut("{id}")]
        public ActionResult<UserDTO> UpdateUser(int id, [FromBody] UserDTO newUser)
        {
            return Ok(userService.UpdateUser(id, newUser));
        }

        [HttpDelete("{id:int}")]
        public IActionResult DeleteUser(int id)
        {
            userService.DeleteUser(id);
            return NoContent();
        }

        [HttpGet("{id:int}")]
        public ActionResult<UserDTO> GetUser(int id)
        {
            return Ok(userService.GetUser(id));    

        }
    }
}
