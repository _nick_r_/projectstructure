﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Shared.DTO.Task;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly ITaskService taskService;

        public TasksController(ITaskService taskService)
        {
            this.taskService = taskService;
        }

        [HttpGet]
        public ActionResult<ICollection<TaskDTO>> GetTasks()
        {
            return Ok(taskService.GetAllTasks());
        }

        [HttpPost]
        public ActionResult<TaskDTO> CreateTask([FromBody] TaskDTO newTask)
        {
            return Ok(taskService.AddTask(newTask));
        }

        [HttpPut("{id}")]
        public ActionResult<TaskDTO> UpdateTask(int id, [FromBody] TaskDTO newTask)
        {
            return Ok(taskService.UpdateTask(id, newTask));
        }

        [HttpDelete("{id:int}")]
        public IActionResult DeleteTask(int id)
        {
            taskService.DeleteTask(id);
            return NoContent();
        }

        [HttpGet("{id:int}")]
        public ActionResult<TaskDTO> GetTask(int id)
        {
            return Ok(taskService.GetTask(id));

        }
    }
}
