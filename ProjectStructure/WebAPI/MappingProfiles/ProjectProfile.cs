﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructure.Shared.DTO.Project;
using ProjectStructure.DataAccess.Models;

namespace ProjectStructure.WebAPI.MappingProfiles
{
    public class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<Project, ProjectDTO>();

            CreateMap<ProjectCreateDTO, Project>();

        }
    }
}
