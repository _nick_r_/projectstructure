﻿using AutoMapper;
using ProjectStructure.DataAccess.Models;
using ProjectStructure.Shared.DTO.Team;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace ProjectStructure.WebAPI.MappingProfiles
{
    public class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<Team, TeamDTO>();

            CreateMap<TeamCreateDTO, Team>();

        }
    }
}
