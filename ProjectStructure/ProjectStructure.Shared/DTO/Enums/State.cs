﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.Shared.DTO.Enums
{
    public enum State
    {
        Started,
        InProgress,
        Testing,
        Finished
    }
}
