﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.Shared.DTO.Team
{
    public class TeamCreateDTO
    {
        public string Name { get; set; }
    }
}
