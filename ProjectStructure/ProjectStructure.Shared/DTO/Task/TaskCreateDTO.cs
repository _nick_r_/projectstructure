﻿using ProjectStructure.Shared.DTO.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace ProjectStructure.Shared.DTO.Task
{
    public class TaskCreateDTO
    {
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public State State { get; set; }
    }
}
