﻿using ProjectStructure.Shared.DTO.Project;
using ProjectStructure.Shared.DTO.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.Shared.DTO.Linq
{
    public class SixthTaskDTO
    {
        public UserDTO user;
        public ProjectDTO lastProject;
        public int numberOfTasksOnTheLast;
        public int numberOfUnfinishedTasks;
    }
}
