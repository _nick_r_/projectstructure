﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LINQ.Models
{
    class Project
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }
        [JsonPropertyName("AuthorId")]
        public int AuthorId { get; set; }
        [JsonPropertyName("teamId")]
        public int TeamId { get; set; }
        [JsonPropertyName("name")]
        public string Name { get; set; }
        [JsonPropertyName("description")]
        public string Description { get; set; }
        [JsonPropertyName("deadline")]
        public DateTime? Deadline { get; set; }
        [JsonPropertyName("createdAt")]
        public DateTime? CreatedAt { get; set; }
    }
}
