﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ.Models.Enums
{
    enum State
    {
        Started,
        InProgress,
        Testing,
        Finished
    }
}
