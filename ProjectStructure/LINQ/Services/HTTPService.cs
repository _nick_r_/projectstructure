﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace LINQ.Services
{
    class HTTPService
    {
        private static HttpClient Client { get; set; } = new HttpClient();

        private static string BaseURL = Settings.baseURL;

        public static async Task<Models.Task[]> GetTasks()
        {
            var response = await Client.GetAsync(BaseURL + "Tasks");           
            return JsonSerializer.Deserialize<Models.Task[]>(await response.Content.ReadAsStringAsync());
        }

        public static async Task<Models.Team[]> GetTeams()
        {
            var response = await Client.GetAsync(BaseURL + "Teams");
            return JsonSerializer.Deserialize<Models.Team[]>(await response.Content.ReadAsStringAsync());
        }

        public static async Task<String> GetUsers()
        {
            var response = await Client.GetAsync(BaseURL + "Users");
            return await response.Content.ReadAsStringAsync();
        }

        public static async Task<Models.Project[]> GetProjects()
        {
            var response = await Client.GetAsync(BaseURL + "Projects");
            return JsonSerializer.Deserialize<Models.Project[]>(await response.Content.ReadAsStringAsync());
        }


    }
}
