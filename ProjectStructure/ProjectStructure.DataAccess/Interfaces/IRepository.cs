﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.DataAccess.Interfaces
{
    public interface IRepository<T>
        where T : class
    {
        IEnumerable<T> GetList(); // получение всех объектов
        T Get(int id); // получение одного объекта по id
        T Create(T item); // создание объекта
        T Update(int id, T item); // обновление объекта
        void Delete(int id); // удаление объекта по id

    }
}
