﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace ProjectStructure.DataAccess.Interfaces.Repositories
{
    public class TaskRepository : IRepository<Models.Task>
    {
        private List<Models.Task> tasks;

        public TaskRepository()
        {
            tasks = new List<Models.Task>();
        }

        public Models.Task Create(Models.Task item)
        {
            tasks.Add(item);
            return item;
        }

        public void Delete(int id)
        {
            tasks.Remove(tasks.Find(x => x.Id == id));
        }

        public Models.Task Get(int id)
        {
            return tasks.Find(x => x.Id == id);
        }

        public IEnumerable<Models.Task> GetList()
        {
            return tasks;
        }

        public Models.Task Update(int id, Models.Task item)
        {
            var index = tasks.FindIndex(x => x.Id == item.Id);
            tasks[index] = item;
            return item;
        }
    }
}
