﻿using ProjectStructure.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace ProjectStructure.DataAccess.Interfaces.Repositories
{
    public class TeamRepository : IRepository<Team>
    {
        private List<Team> teams;

        public TeamRepository()
        {
            teams = new List<Team>();
        }

        public Team Create(Team item)
        {
            teams.Add(item);
            return item;
        }

        public void Delete(int id)
        {
            teams.Remove(teams.Find(x => x.Id == id));
        }

        public Team Get(int id)
        {
            return teams.Find(x => x.Id == id);
        }

        public IEnumerable<Team> GetList()
        {
            return teams;
        }

        public Team Update(int id, Team item)
        {
            var index = teams.FindIndex(x => x.Id == item.Id);
            teams[index] = item;

            return item;
        }
    }
}
