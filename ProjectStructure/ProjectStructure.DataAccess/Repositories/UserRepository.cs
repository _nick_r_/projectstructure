﻿using ProjectStructure.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace ProjectStructure.DataAccess.Interfaces.Repositories
{
    public class UserRepository : IRepository<User>
    {
        private List<User> users;

        public UserRepository()
        {
            users = new List<User>();
        }

        public User Create(User item)
        {
            users.Add(item);

            return item;
        }

        public void Delete(int id)
        {
            users.Remove(users.Find(x => x.Id == id));
        }

        public User Get(int id)
        {
            return users.Find(x => x.Id == id);
        }

        public IEnumerable<User> GetList()
        {
            return users;
        }

        public User Update(int id, User item)
        {
            var index = users.FindIndex(x => x.Id == item.Id);
            users[index] = item;

            return item;
        }
    }
}
