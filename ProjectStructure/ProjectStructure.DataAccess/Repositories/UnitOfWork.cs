﻿using ProjectStructure.DataAccess.Interfaces;
using ProjectStructure.DataAccess.Interfaces.Repositories;
using ProjectStructure.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.DataAccess.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private UserRepository userRepository;
        private TeamRepository teamRepository;
        private TaskRepository taskRepository;
        private ProjectRepository projectRepository;

        public IRepository<User> Users
        {
            get
            {
                if (userRepository == null)
                    userRepository = new UserRepository();
                return userRepository;
            }
        }

        public IRepository<Team> Teams
        {
            get
            {
                if (teamRepository == null)
                    teamRepository = new TeamRepository();
                return teamRepository;
            }
        }

        public IRepository<Models.Task> Tasks
        {
            get
            {
                if (taskRepository == null)
                    taskRepository = new TaskRepository();
                return taskRepository;
            }
        }

        public IRepository<Project> Projects
        {
            get
            {
                if (projectRepository == null)
                    projectRepository = new ProjectRepository();
                return projectRepository;
            }
        }
    }
}
