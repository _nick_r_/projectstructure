﻿using ProjectStructure.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace ProjectStructure.DataAccess.Interfaces.Repositories
{
    public class ProjectRepository : IRepository<Project>
    {
        private List<Project> projects;

        public ProjectRepository()
        {
            projects = new List<Project>();
        }

        public Project Create(Project item)
        {
            projects.Add(item);
            return item;
        }

        public void Delete(int id)
        {
            projects.Remove(projects.Find(x => x.Id == id));
        }

        public Project Get(int id)
        {
            return projects.Find(x => x.Id == id);
        }

        public IEnumerable<Project> GetList()
        {
            return projects;
        }

        public Project Update(int id, Project item)
        {
            var index = projects.FindIndex(x => x.Id == item.Id);
            projects[index] = item;
            return item;
        }
    }
}
